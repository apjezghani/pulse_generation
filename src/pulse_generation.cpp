#include "simulation.h"
#include <iostream>
#include <time.h>
#include <stdio.h>
using namespace std;

int main(int argc, char** argv)
{
  if(argc<2)
    {
      cerr<<"You need to specify a pulse amplitude! Use: ./bin/pulse_generation AMP"<<endl;
      return 1;
    }
  time_t timer;
  int SEED=time(&timer); //set a value rather than using the time if you want a manual seed for reproducing results
  default_random_engine generator;
  generator.seed(SEED);
  uniform_real_distribution<double> distribution(0.0,1.0);
  short test[3500];
  for(int j=0; j<10000; j++)
    {
      gen_synth_pulse(atoi(argv[1]),3500,test,generator,distribution);
      superimpose_noise(3500,test,generator,distribution);
      for(int i=0; i<3500; i++)
	cout<<test[i]<<endl;
    }
  return 0;
}
